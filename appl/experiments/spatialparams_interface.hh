// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup BoundaryTests
 * \brief The spatial parameters class.
 */

#ifndef DUMUX_CONSERVATION_SPATIAL_PARAMS_INTERFACE_HH
#define DUMUX_CONSERVATION_SPATIAL_PARAMS_INTERFACE_HH

#include <dumux/material/spatialparams/fv.hh>
#include "../../src/drops/materiallaw.hh"

namespace Dumux {

/*!
 * \ingroup BoundaryTests
 *
 * \brief The spatial parameters class
 */
template<class FVGridGeometry, class Scalar>
class InterfaceSpatialParams
: public FVSpatialParams<FVGridGeometry, Scalar,
                         InterfaceSpatialParams<FVGridGeometry, Scalar>>
{
    using GridView = typename FVGridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using FVElementGeometry = typename FVGridGeometry::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using ThisType = InterfaceSpatialParams<FVGridGeometry, Scalar>;
    using ParentType = FVSpatialParams<FVGridGeometry, Scalar, ThisType>;

    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    using EffectiveLaw = DropMaterialLaw<Scalar>;

public:
    using MaterialLaw = EffectiveLaw;
    using MaterialLawParams = typename MaterialLaw::Params;
    using PermeabilityType = Scalar;

    struct poreClass {Scalar meanPoreRadius; Scalar percentage; };

    InterfaceSpatialParams(std::shared_ptr<const FVGridGeometry> fvGridGeometry)
        : ParentType(fvGridGeometry)
    {
        permeability_ = getParam<Scalar>("SpatialParams.Permeability");
        porosityPM_ = getParam<Scalar>("SpatialParams.Porosity");
        alphaBJ_ = getParam<Scalar>("SpatialParams.AlphaBJ");

        contactAngle_ = getParam<Scalar>("SpatialParams.Theta");
        surfaceTension_ = getParam<Scalar>("SpatialParams.SurfaceTension");
    }

    /*!
     * \brief Function for defining the (intrinsic) permeability \f$[m^2]\f$.
     *
     * \param globalPos The global position
     * \return the intrinsic permeability
     */
    PermeabilityType permeabilityAtPos(const GlobalPosition& globalPos) const
    { return permeability_; }

    /*! \brief Defines the porosity in [-].
     *
     * \param globalPos The global position
     */
    Scalar porosityAtPos(const GlobalPosition& globalPos) const
    { return 1.0; }

    /*! \brief Defines the porosity in [-].
     */
    Scalar porosityPM() const
    { return porosityPM_; }

    /*! \brief Defines the Beavers-Joseph coefficient in [-].
     *
     * \param globalPos The global position
     */
    Scalar beaversJosephCoeffAtPos(const GlobalPosition& globalPos) const
    { return alphaBJ_; }

     /*
      * \brief Returns the contact angle.
      */
     Scalar contactAngle() const
     { return contactAngle_; }

    /*!
     * \brief Returns the parameter object for the Brooks-Corey material law.
     *
     * In this test, we use element-wise distributed material parameters.
     *
     * \param element The current element
     * \param scv The sub-control volume inside the element.
     * \param elemSol The solution at the dofs connected to the element.
     * \return The material parameters object
     */
    template<class ElementSolutionVector>
    const MaterialLawParams& materialLawParams(const Element& element,
                                               const SubControlVolume& scv,
                                               const ElementSolutionVector& elemSol) const
    { return params_; }

    /*!
     * \brief Function for defining which phase is to be considered as the wetting phase.
     *
     * \param globalPos The global position
     * \return The wetting phase index
     */
    template<class FluidSystem>
    int wettingPhaseAtPos(const GlobalPosition& globalPos) const
    { return FluidSystem::phase1Idx; } // gas phase (air)

private:
    Scalar permeability_;
    Scalar porosityPM_;
    Scalar alphaBJ_;
    Scalar contactAngle_;
    Scalar surfaceTension_;
    MaterialLawParams params_;
    static constexpr Scalar eps_ = 1.0e-7;
};

} // end namespace Dumux

#endif
