# First experiment (Dec 2019)

## Properties

* Pore: 1.5mm diameter

* Material: Teflon (hydrophobic)

* Fluid: Water with potassium iodide (1:6), surface tension = 0.0681 N/m

* Filling rate: 50e-6 l/min

## Simulation

* Evaporation not yet taken into account (isothermal)

* Several pores within interface domain, sum up fluxes, divide drop volume

* Aim: Compare temporal evolution and drop volume (approximately!)

* [Teflon parameters](https://www.treborintl.com/content/properties-molded-ptfe)
