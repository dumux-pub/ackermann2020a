add_input_file_links()

dune_add_test(NAME test_drops_lateral
              LABELS multidomain drops
              SOURCES main.cc
              COMPILE_DEFINITIONS NONISOTHERMAL=0 LATERAL=1
              CMAKE_GUARD HAVE_UMFPACK
              )

dune_add_test(NAME test_drops_lateral_ni
              LABELS multidomain drops
              SOURCES main.cc
              COMPILE_DEFINITIONS NONISOTHERMAL=1 LATERAL=1
              CMAKE_GUARD HAVE_UMFPACK
              )

