// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup BoundaryTests
 * \brief A simple Darcy test problem (cell-centered finite volume method).
 */

#ifndef DUMUX_INTERFACE_SUBPROBLEM_HH
#define DUMUX_INTERFACE_SUBPROBLEM_HH

#include <dune/grid/yaspgrid.hh>

#include <dumux/discretization/cctpfa.hh>

#include <src/drops/model.hh>
#include <src/drops/problem.hh>

#include "spatialparams_interface.hh"

namespace Dumux {
template <class TypeTag>
class InterfaceSubProblem;

namespace Properties {
// Create new type tags
namespace TTag {
#if !NONISOTHERMAL
struct InterfaceTwoPTwoC { using InheritsFrom = std::tuple<Drop, CCTpfaModel>; };
#else
struct InterfaceTwoPTwoC { using InheritsFrom = std::tuple<DropNI, CCTpfaModel>; };
#endif
} // end namespace TTag

// Set the problem property
template<class TypeTag>
struct Problem<TypeTag, TTag::InterfaceTwoPTwoC> { using type = Dumux::InterfaceSubProblem<TypeTag>; };

// Set the grid type --> one dimension less than Stokes and Darcy problem grids!
template<class TypeTag>
struct Grid<TypeTag, TTag::InterfaceTwoPTwoC> { using type = Dune::YaspGrid<1, Dune::EquidistantOffsetCoordinates<GetPropType<TypeTag, Properties::Scalar>, 1> >; };

template<class TypeTag>
struct EnableGridVolumeVariablesCache<TypeTag, TTag::InterfaceTwoPTwoC> { static constexpr bool value = true; };

template<class TypeTag>
struct ReplaceCompEqIdx<TypeTag, TTag::InterfaceTwoPTwoC> { static constexpr int value = 3; };

// use formulation based on mass fractions
template<class TypeTag>
struct UseMoles<TypeTag, TTag::InterfaceTwoPTwoC> { static constexpr bool value = false; };

template<class TypeTag>
struct SpatialParams<TypeTag, TTag::InterfaceTwoPTwoC>
{
    using FVGridGeometry = GetPropType<TypeTag, Properties::FVGridGeometry>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using type = InterfaceSpatialParams<FVGridGeometry, Scalar>;
};

template<class TypeTag>
struct Formulation<TypeTag, TTag::InterfaceTwoPTwoC>
{ static constexpr auto value = TwoPFormulation::p1s0; };
} // end namespace Properties

template <class TypeTag>
class InterfaceSubProblem : public DropProblem<TypeTag>
{
    using ParentType = DropProblem<TypeTag>;
    using GridView = GetPropType<TypeTag, Properties::GridView>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using NumEqVector = GetPropType<TypeTag, Properties::NumEqVector>;
    using BoundaryTypes = GetPropType<TypeTag, Properties::BoundaryTypes>;
    using VolumeVariables = GetPropType<TypeTag, Properties::VolumeVariables>;
    using FVElementGeometry = typename GetPropType<TypeTag, Properties::FVGridGeometry>::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;
    using FVGridGeometry = GetPropType<TypeTag, Properties::FVGridGeometry>;
    using ElementVolumeVariables = typename GetPropType<TypeTag, Properties::GridVolumeVariables>::LocalView;

    using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;

    // copy some indices for convenience
    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;
    enum {
        // primary variable indices
        conti0EqIdx = Indices::conti0EqIdx,
        pressureIdx = Indices::pressureIdx,
        switchIdx = Indices::switchIdx,
    };

    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    using CouplingManager = GetPropType<TypeTag, Properties::CouplingManager>;

public:
    InterfaceSubProblem(std::shared_ptr<const FVGridGeometry> fvGridGeometry,
                        std::shared_ptr<CouplingManager> couplingManager)
    : ParentType(fvGridGeometry, "Interface"), eps_(1e-7), couplingManager_(couplingManager)
    {
        pressure_ = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.Pressure");
        saturation_ = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.Saturation");
        temperature_ = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.Temperature");
        initialPhasePresence_ = getParamFromGroup<int>(this->paramGroup(), "Problem.InitPhasePresence");
        problemName_  =  getParam<std::string>("Vtk.OutputName") + "_" + getParamFromGroup<std::string>(this->paramGroup(), "Problem.Name");
        extrusionFactor1d3d_ = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.ExtrusionFactor");
        extrusionFactor2d3d_ = getParam<Scalar>("Darcy.Problem.ExtrusionFactor");
        maxSat_ = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.MaximumSaturation");
        deltaP_ = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.PressureDifference");
    }

    /*!
     * \brief The problem name.
     */
    const std::string& name() const
    {
        return problemName_;
    }

    /*!
     * \name Simulation steering
     */
    // \{

    /*!
     * \brief Returns true if a restart file should be written to disk.
     */
    bool shouldWriteRestartFile() const
    { return false; }

    /*!
     * \name Problem parameters
     */
    // \{

    bool shouldWriteOutput() const // define output
    { return true; }


     /*!
     * \brief Return how much the domain is extruded at a given position.
     *
     * This means the factor by which a lower-dimensional (1D or 2D)
     * entity needs to be expanded to get a full dimensional cell. The
     * default is 1.0 which means that 1D problems are actually
     * thought as pipes with a cross section of 1 m^2 and 2D problems
     * are assumed to extend 1 m to the back.
     */
    Scalar extrusionFactorAtPos(const GlobalPosition &globalPos) const
    { return extrusionFactor1d3d_; }

    /*!
     * \brief Returns the temperature within the domain in [K].
     *
     */
    Scalar temperature() const
    { return temperature_; }
    // \}

    Scalar dropVolume(const int elemIdx) const
    { return couplingManager().dropManager().getDropVolume(elemIdx); }

     /*!
     * \name Boundary conditions
     */
    // \{

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary segment.
     *
     * \param element The finite element
     * \param scvf The sub control volume face
     */
    BoundaryTypes boundaryTypes(const Element& element,
                                const SubControlVolumeFace& scvf) const
    {
        BoundaryTypes values;
        values.setAllDirichlet();

        return values;
    }

    /*!
     * \brief Evaluates the boundary conditions for a Dirichlet control volume.
     *
     * \param element The element for which the Dirichlet boundary condition is set
     * \param scvf The boundary sub control volume face
     *
     * For this method, the \a values parameter stores primary variables.
     */
    PrimaryVariables dirichlet(const Element &element, const SubControlVolumeFace &scvf) const
    {
        PrimaryVariables values(0.0);
        values = initial(element);

        if (onLeftBoundary_(scvf.center()))
            values[Indices::pressureIdx] = pressure_ + deltaP_/2.0 - 2 * deltaP_/10.0;

        if (onRightBoundary_(scvf.center()))
            {
                values[Indices::pressureIdx] = pressure_ - deltaP_/2.0 + 2 * deltaP_/10.0;
                values[Indices::switchIdx] = 0.5001;
                values.setState(3);
            }

        return values;
    }

    /*!
     * \brief Evaluates the boundary conditions for a Neumann control volume.
     *
     * \param element The element for which the Neumann boundary condition is set
     * \param fvGeometry The fvGeometry
     * \param elemVolVars The element volume variables
     * \param scvf The boundary sub control volume face
     *
     * For this method, the \a values variable stores primary variables.
     */
    NumEqVector neumann(const Element& element,
                        const FVElementGeometry& fvGeometry,
                        const ElementVolumeVariables& elemVolVars,
                        const SubControlVolumeFace& scvf) const
    {
        NumEqVector values(0.0);
        return values;
    }

    // \}

    /*!
     * \name Volume terms
     */
    // \{
    /*!
     * \brief Evaluates the source term for all phases within a given
     *        sub control volume.
     *
     * \param element The element for which the source term is set
     * \param fvGeometry The fvGeometry
     * \param elemVolVars The element volume variables
     * \param scv The sub control volume
     *
     * For this method, the \a values variable stores the rate mass
     * of a component is generated or annihilated per volume
     * unit. Positive values mean that mass is created, negative ones
     * mean that it vanishes.
     */
     NumEqVector source(const Element &element,
                        const FVElementGeometry& fvGeometry,
                        const ElementVolumeVariables& elemVolVars,
                        const SubControlVolume &scv) const
     {
         NumEqVector values(0.0);

         // coupling fluxes FF/IF and PM/IF, in kg/(m²s), J/(m²s)
         const auto massSource = couplingManager().couplingData().massCouplingCondition(element, fvGeometry, elemVolVars, scv);

         values[Indices::conti0EqIdx] -= massSource[0] * (scv.volume() * extrusionFactor2d3d_); // in kg/s
         values[Indices::conti0EqIdx + 1] -= massSource[1] * (scv.volume() * extrusionFactor2d3d_); // in kg/s
#if NONISOTHERMAL
         values[Indices::energyEqIdx] -= couplingManager().couplingData().energyCouplingCondition(element, fvGeometry, elemVolVars, scv)
                                        * (scv.volume() * extrusionFactor2d3d_); // in J/s
#endif

         // detached drops in kg/(m²s), J/(m²s)
         const Scalar density = elemVolVars[scv].density(0);
         const Scalar massFrac = elemVolVars[scv].massFraction(0, 0); // liquidPhase, H2OComp

         // evaluate detached drop volumes as sinks (computed at the end of the previous time step)
#if NONISOTHERMAL
         const Scalar internalEnergy = elemVolVars[scv].internalEnergy(0); // liquidPhase
#else
         const Scalar internalEnergy = 0.0;
#endif
         values -= couplingManager().dropManager().detachedDrops(scv.dofIndex(), density, massFrac, internalEnergy);

         // conversion to kg/s: * scv.volume(m) * EF2d3d(m); conversion to kg/(m^3/s): / (scv.volume(m) * EF1d3d(m^2))
         // TODO implemented for 1D interface domain only!
         for (auto& value : values)
            value /= scv.volume() * extrusionFactor1d3d_;

         return values;
     }


    // \}

    /*!
     * \brief Evaluates the initial value for a control volume.
     *
     * \param element The element
     *
     * For this method, the \a priVars parameter stores primary
     * variables.
     */
    PrimaryVariables initial(const Element &element) const
    {
        PrimaryVariables values(0.0);
        values.setState(initialPhasePresence_);
        values[pressureIdx] = pressure_;
        values[switchIdx] = saturation_;
#if NONISOTHERMAL
        values[Indices::temperatureIdx] = temperature_;
#endif
        return values;
    }

    // \}

    /*!
     * \brief Get the coupling manager
     */
    const CouplingManager& couplingManager() const
    { return *couplingManager_; }

    const Scalar maxSaturation() const
    { return maxSat_; }

private:
    bool onLeftBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[0] < this->fvGridGeometry().bBoxMin()[0] + eps_; }

    bool onRightBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[0] > this->fvGridGeometry().bBoxMax()[0] - eps_; }

    Scalar eps_;

    Scalar pressure_;
    Scalar saturation_;
    Scalar temperature_;
    int initialPhasePresence_;
    std::string problemName_;
    Scalar extrusionFactor1d3d_;
    Scalar extrusionFactor2d3d_;
    Scalar maxSat_, deltaP_;

    std::shared_ptr<CouplingManager> couplingManager_;
};
} // end namespace Dumux

#endif //DUMUX_INTERFACE_SUBPROBLEM_HH
