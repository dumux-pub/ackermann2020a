// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup Fluidmatrixinteractions
 */
#ifndef DROPS_MATERIAL_LAW_PARAMS_HH
#define DROPS_MATERIAL_LAW_PARAMS_HH

#include <dune/common/float_cmp.hh>

namespace Dumux {

/*!
 * \ingroup Fluidmatrixinteractions
 * \brief   Parameters that are necessary for the \em regularization of
 *          VanGenuchten "material law".
 */
template<class ScalarT>
class DropMaterialLawParams
{
public:
    using Scalar = ScalarT;

    /*!
    * \brief Set the threshold saturation above which lateral fluxes take place.
    *
    */
    void setSlMax(Scalar slMax)
    { slMax_ = slMax; }

    /*!
    * \brief Threshold saturation above which lateral fluxes take place.
    */
    Scalar slMax() const
    { return slMax_; }


private:
    Scalar slMax_;

};
} // namespace Dumux

#endif
