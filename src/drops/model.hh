// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup DropModel
 *
 */

#ifndef DUMUX_DROP_MODEL_HH
#define DUMUX_DROP_MODEL_HH

#include <dumux/common/properties.hh>
#include <dumux/porousmediumflow/properties.hh>
#include <dumux/porousmediumflow/2pnc/model.hh>
#include <dumux/porousmediumflow/compositional/switchableprimaryvariables.hh>

#include <dumux/porousmediumflow/nonisothermal/model.hh>
#include <dumux/porousmediumflow/nonisothermal/indices.hh>
#include <dumux/porousmediumflow/nonisothermal/iofields.hh>

#include <dumux/porousmediumflow/2p/formulation.hh>
#include <dumux/porousmediumflow/2pnc/indices.hh>
#include "volumevariables.hh"

#include "localresidual.hh"
#include "iofields.hh"

namespace Dumux
{
/*!
 * \ingroup DropModel
 * \brief Specifies a number properties of drop models.
 */
template<int nComp, bool useMol, TwoPFormulation formulation, int repCompEqIdx = nComp>
struct DropModelTraits
{
    using Indices = TwoPNCIndices;

    static constexpr int numEq() { return 2; }
    static constexpr int numFluidPhases() { return 2; }
    static constexpr int numFluidComponents() {return 2; }
    static constexpr int replaceCompEqIdx() { return repCompEqIdx; }

    static constexpr bool enableAdvection() { return true; }
    static constexpr bool enableMolecularDiffusion() { return true; }
    static constexpr bool enableEnergyBalance() { return false; }

    static constexpr bool useMoles() { return useMol; }

    static constexpr TwoPFormulation priVarFormulation()
    { return formulation; }
};

/*!
 * \ingroup DropModel
 * \brief Traits class for the drop model.
 *
 * \tparam PV The type used for primary variables
 * \tparam FSY The fluid system type
 * \tparam FST The fluid state type
 * \tparam SSY The solid system type
 * \tparam SST The solid state type
 * \tparam PT The type used for permeabilities
 * \tparam MT The model traits
 * \tparam SR The class used for reconstruction of
 *            non-wetting phase saturations in scvs
 */
template<class PV, class FSY, class FST,class SSY, class SST, class PT, class MT> //, class SR>
struct DropVolumeVariablesTraits
{
    using PrimaryVariables = PV;
    using FluidSystem = FSY;
    using FluidState = FST;
    using SolidSystem = SSY;
    using SolidState = SST;
    using PermeabilityType = PT;
    using ModelTraits = MT;
};

class DropIOFields;

////////////////////////////////
// properties
////////////////////////////////
namespace Properties {

//////////////////////////////////////////////////////////////////
// Type tags
//////////////////////////////////////////////////////////////////
// Create new type tags
namespace TTag {
struct Drop { using InheritsFrom = std::tuple<TwoP>; };
struct DropNI { using InheritsFrom = std::tuple<Drop>; };
} // end namespace TTag

///////////////////////////////////////////////////////////////////////////
// properties for the isothermal drop model
///////////////////////////////////////////////////////////////////////////
//! The primary variables vector for the drop model
template<class TypeTag>
struct PrimaryVariables<TypeTag, TTag::Drop>
{
private:
    using PrimaryVariablesVector = Dune::FieldVector<GetPropType<TypeTag, Properties::Scalar>,
                                                     GetPropType<TypeTag, Properties::ModelTraits>::numEq()>;
public:
    using type = SwitchablePrimaryVariables<PrimaryVariablesVector, int>;
};

//! Set the volume variables property
template<class TypeTag>
struct VolumeVariables<TypeTag, TTag::Drop>
{
private:
    using PV = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using FSY = GetPropType<TypeTag, Properties::FluidSystem>;
    using FST = GetPropType<TypeTag, Properties::FluidState>;
    using SSY = GetPropType<TypeTag, Properties::SolidSystem>;
    using SST = GetPropType<TypeTag, Properties::SolidState>;
    using MT = GetPropType<TypeTag, Properties::ModelTraits>;
    using PT = typename GetPropType<TypeTag, Properties::SpatialParams>::PermeabilityType;

    using Traits = DropVolumeVariablesTraits<PV, FSY, FST, SSY, SST, PT, MT>;//, SR>;
public:
    using type = DropsVolumeVariables<Traits>;
};

//! The base model traits class
template<class TypeTag>
struct BaseModelTraits<TypeTag, TTag::Drop>
{
    private:
        using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
    public:
        using type = DropModelTraits<FluidSystem::numComponents,
                                     getPropValue<TypeTag, Properties::UseMoles>(),
                                     getPropValue<TypeTag, Properties::Formulation>(),
                                     getPropValue<TypeTag, Properties::ReplaceCompEqIdx>() >;
};

template<class TypeTag>
struct ModelTraits<TypeTag, TTag::Drop> { using type = GetPropType<TypeTag, Properties::BaseModelTraits>; };

//! Set the vtk output fields specific to the drop model
template<class TypeTag>
struct IOFields<TypeTag, TTag::Drop> { using type = DropIOFields; };

template<class TypeTag>
struct LocalResidual<TypeTag, TTag::Drop> { using type = DropLocalResidual<TypeTag>; };

template<class TypeTag>
struct ReplaceCompEqIdx<TypeTag, TTag::Drop> { static constexpr int value = GetPropType<TypeTag, Properties::FluidSystem>::numComponents; }; //!< Per default, no component mass balance is replaced

 //!< Set the default formulation to pwsn
template<class TypeTag>
struct Formulation<TypeTag, TTag::Drop>
{ static constexpr auto value = TwoPFormulation::p0s1; };

template<class TypeTag>
struct UseMoles<TypeTag, TTag::Drop> { static constexpr bool value = true; };

template<class TypeTag>
struct EffectiveDiffusivityModel<TypeTag, TTag::Drop> { using type = DiffusivityMillingtonQuirk<GetPropType<TypeTag, Properties::Scalar>>; };

//! The drop model uses the compositional fluid state
template<class TypeTag>
struct FluidState<TypeTag, TTag::Drop>
{
private:
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
public:
    using type = CompositionalFluidState<Scalar, FluidSystem>;
};

////////////////////////////////////////////////////////
// properties for the non-isothermal drop model
////////////////////////////////////////////////////////

//! The non-isothermal model traits class
template<class TypeTag>
struct ModelTraits<TypeTag, TTag::DropNI> { using type = PorousMediumFlowNIModelTraits<GetPropType<TypeTag, Properties::BaseModelTraits>>; };

//! Set the vtk output fields specific to the non-isothermal drop model
template<class TypeTag>
struct IOFields<TypeTag, TTag::DropNI> { using type = EnergyIOFields<DropIOFields>; };

//! Somerton is used as default model to compute the effective thermal heat conductivity
template<class TypeTag>
struct ThermalConductivityModel<TypeTag, TTag::DropNI>
{
private:
   using Scalar = GetPropType<TypeTag, Properties::Scalar>;
public:
   using type = ThermalConductivitySomerton<Scalar>;
};

} // end namespace Properties
} // end namespace Dumux

#endif
