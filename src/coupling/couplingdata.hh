// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup StokesDropsDarcyCoupling
 * \copydoc Dumux::StokesDropsDarcyCouplingData
 */

#ifndef DUMUX_STOKES_DROPS_DARCY_COUPLINGDATA_HH
#define DUMUX_STOKES_DROPS_DARCY_COUPLINGDATA_HH

#include <numeric>

#include <dumux/common/properties.hh>
#include <dumux/common/math.hh>
#include <dumux/discretization/method.hh>
#include <dumux/multidomain/couplingmanager.hh>

namespace Dumux {

/*!
 * \ingroup StokesDarcyCoupling
 * \brief This structs holds a set of options which allow to modify the Stokes-Darcy
 *        coupling mechanism during runtime.
 */
struct StokesDarcyCouplingOptions
{
    /*!
     * \brief Defines which kind of averanging of diffusion coefficients
     *        (molecular diffusion or thermal conductance) at the interface
     *        between free flow and porous medium shall be used.
     */
    enum class DiffusionCoefficientAveragingType
    {
        harmonic, arithmethic, ffOnly, pmOnly
    };

    /*!
     * \brief Convenience function to convert user input given as std::string to the corresponding enum class used for chosing the type
     *        of averaging of the diffusion/conduction parameter at the interface between the two domains.
     */
    static DiffusionCoefficientAveragingType stringToEnum(DiffusionCoefficientAveragingType, const std::string& diffusionCoefficientAveragingType)
    {
        if (diffusionCoefficientAveragingType == "Harmonic")
            return DiffusionCoefficientAveragingType::harmonic;
        else if (diffusionCoefficientAveragingType == "Arithmethic")
            return DiffusionCoefficientAveragingType::arithmethic;
        else if (diffusionCoefficientAveragingType == "FreeFlowOnly")
            return DiffusionCoefficientAveragingType::ffOnly;
        else if (diffusionCoefficientAveragingType == "PorousMediumOnly")
            return DiffusionCoefficientAveragingType::pmOnly;
        else
            DUNE_THROW(Dune::IOError, "Unknown DiffusionCoefficientAveragingType");
    }

};

/*!
 * \ingroup StokesDarcyCoupling
 * \brief This structs helps to check if the two sub models use the same fluidsystem.
 *        Specialization for the case of using an adapter only for the free-flow model.
 * \tparam FFFS The free-flow fluidsystem
 * \tparam PMFS The porous-medium flow fluidsystem
 */
template<class FFFS, class PMFS>
struct IsSameFluidSystem
{
    static_assert(FFFS::numPhases == 1, "Only single-phase fluidsystems may be used for free flow.");
    static constexpr bool value = std::is_same<typename FFFS::MultiPhaseFluidSystem, PMFS>::value;
};

/*!
 * \ingroup StokesDarcyCoupling
 * \brief This structs helps to check if the two sub models use the same fluidsystem.
 * \tparam FS The fluidsystem
 */
template<class FS>
struct IsSameFluidSystem<FS, FS>
{
    static_assert(FS::numPhases == 1, "Only single-phase fluidsystems may be used for free flow.");
    static constexpr bool value = std::is_same<FS, FS>::value; // always true
};

// forward declaration
template <class TypeTag, DiscretizationMethod discMethod>
class FicksLawImplementation;

/*!
 * \ingroup StokesDarcyCoupling
 * \brief This structs indicates that Fick's law is not used for diffusion.
 * \tparam DiffLaw The diffusion law.
 */
template<class DiffLaw>
struct IsFicksLaw : public std::false_type {};

/*!
 * \ingroup StokesDarcyCoupling
 * \brief This structs indicates that Fick's law is used for diffusion.
 * \tparam DiffLaw The diffusion law.
 */
template<class T, DiscretizationMethod discMethod>
struct IsFicksLaw<FicksLawImplementation<T, discMethod>> : public std::true_type {};

/*!
 * \ingroup StokesDarcyCoupling
 * \brief Helper struct to choose the correct index for phases and components. This is need if the porous-medium-flow model
          features more fluid phases than the free-flow model.
 * \tparam stokesIdx The domain index of the free-flow model.
 * \tparam interfaceIdx The domain index of the interface model.
 * \tparam darcyIdx The domain index of the porous-medium-flow model.
 * \tparam FFFS The free-flow fluidsystem.
 * \tparam hasAdapter Specifies whether an adapter class for the fluidsystem is used.
 */
template<std::size_t stokesIdx, std::size_t interfaceIdx, std::size_t darcyIdx, class FFFS, bool hasAdapter>
struct IndexHelper;

/*!
 * \ingroup StokesDarcyCoupling
 * \brief Helper struct to choose the correct index for phases and components. This is need if the porous-medium-flow model
          features more fluid phases than the free-flow model. Specialization for the case that no adapter is used.
 * \tparam stokesIdx The domain index of the free-flow model.
 * \tparam darcyIdx The domain index of the porous-medium-flow model.
 * \tparam FFFS The free-flow fluidsystem.
 */
template<std::size_t stokesIdx, std::size_t interfaceIdx, std::size_t darcyIdx, class FFFS>
struct IndexHelper<stokesIdx, interfaceIdx, darcyIdx, FFFS, false>
{
    /*!
     * \brief No adapter is used, just return the input index.
     */
    template<std::size_t i>
    static constexpr auto couplingPhaseIdx(Dune::index_constant<i>, int coupledPhaseIdx = 0)
    { return coupledPhaseIdx; }

    /*!
     * \brief No adapter is used, just return the input index.
     */
    template<std::size_t i>
    static constexpr auto couplingCompIdx(Dune::index_constant<i>, int coupledCompdIdx)
    { return coupledCompdIdx; }
};

/*!
 * \ingroup StokesDarcyCoupling
 * \brief Helper struct to choose the correct index for phases and components. This is need if the porous-medium-flow model
          features more fluid phases than the free-flow model. Specialization for the case that a adapter is used.
 * \tparam stokesIdx The domain index of the free-flow model.
 * \tparam darcyIdx The domain index of the porous-medium-flow model.
 * \tparam FFFS The free-flow fluidsystem.
 */
template<std::size_t stokesIdx, std::size_t interfaceIdx, std::size_t darcyIdx, class FFFS>
struct IndexHelper<stokesIdx, interfaceIdx, darcyIdx, FFFS, true>
{
    /*!
     * \brief The free-flow model always uses phase index 0.
     */
    static constexpr auto couplingPhaseIdx(Dune::index_constant<stokesIdx>, int coupledPhaseIdx = 0)
    { return 0; }

    /*!
     * \brief The phase index of the porous-medium-flow model is given by the adapter fluidsytem (i.e., user input).
     */
    static constexpr auto couplingPhaseIdx(Dune::index_constant<interfaceIdx>, int coupledPhaseIdx = 0)
    { return FFFS::multiphaseFluidsystemPhaseIdx; }

    /*!
     * \brief The phase index of the porous-medium-flow model is given by the adapter fluidsytem (i.e., user input).
     */
    static constexpr auto couplingPhaseIdx(Dune::index_constant<darcyIdx>, int coupledPhaseIdx = 0)
    { return FFFS::multiphaseFluidsystemPhaseIdx; }

    /*!
     * \brief The free-flow model does not need any change of the component index.
     */
    static constexpr auto couplingCompIdx(Dune::index_constant<stokesIdx>, int coupledCompdIdx)
    { return coupledCompdIdx; }

    /*!
     * \brief The component index of the interface model is mapped by the adapter fluidsytem.
     */
    static constexpr auto couplingCompIdx(Dune::index_constant<interfaceIdx>, int coupledCompdIdx)
    { return FFFS::compIdx(coupledCompdIdx); }

    /*!
     * \brief The component index of the porous-medium-flow model is mapped by the adapter fluidsytem.
     */
    static constexpr auto couplingCompIdx(Dune::index_constant<darcyIdx>, int coupledCompdIdx)
    { return FFFS::compIdx(coupledCompdIdx); }
};

template<class MDTraits, class CouplingManager, bool enableEnergyBalance, bool isCompositional>
class StokesDropsDarcyCouplingDataImplementation;

/*!
* \ingroup BoundaryCoupling
* \brief Data for the coupling of a Darcy model (cell-centered finite volume)
*        with a (Navier-)Stokes model (staggerd grid).
*/
template<class MDTraits, class CouplingManager>
using StokesDropsDarcyCouplingData = StokesDropsDarcyCouplingDataImplementation<MDTraits, CouplingManager,
                                                                      GetPropType<typename MDTraits::template SubDomain<0>::TypeTag, Properties::ModelTraits>::enableEnergyBalance(),
                                                                      (GetPropType<typename MDTraits::template SubDomain<0>::TypeTag, Properties::ModelTraits>::numFluidComponents() > 1)>;

/*!
 * \ingroup StokesDarcyCoupling
 * \brief A base class which provides some common methods used for Stokes-Darcy coupling.
 */
template<class MDTraits, class CouplingManager>
class StokesDropsDarcyCouplingDataImplementationBase
{
    using Scalar = typename MDTraits::Scalar;

    template<std::size_t id> using SubDomainTypeTag = typename MDTraits::template SubDomain<id>::TypeTag;
    template<std::size_t id> using FVGridGeometry = GetPropType<SubDomainTypeTag<id>, Properties::FVGridGeometry>;
    template<std::size_t id> using Element = typename FVGridGeometry<id>::GridView::template Codim<0>::Entity;
    template<std::size_t id> using FVElementGeometry = typename FVGridGeometry<id>::LocalView;
    template<std::size_t id> using SubControlVolume = typename FVGridGeometry<id>::LocalView::SubControlVolume;
    template<std::size_t id> using SubControlVolumeFace = typename FVGridGeometry<id>::LocalView::SubControlVolumeFace;
    template<std::size_t id> using Indices = typename GetPropType<SubDomainTypeTag<id>, Properties::ModelTraits>::Indices;
    template<std::size_t id> using ElementVolumeVariables = typename GetPropType<SubDomainTypeTag<id>, Properties::GridVolumeVariables>::LocalView;
    template<std::size_t id> using VolumeVariables = typename GetPropType<SubDomainTypeTag<id>, Properties::GridVolumeVariables>::VolumeVariables;
    template<std::size_t id> using FluidSystem  = GetPropType<SubDomainTypeTag<id>, Properties::FluidSystem>;
    template<std::size_t id> using ModelTraits  = GetPropType<SubDomainTypeTag<id>, Properties::ModelTraits>;

    using GlobalPosition = typename Element<0>::Geometry::GlobalCoordinate;

    static constexpr auto stokesIdx = CouplingManager::stokesIdx;
    static constexpr auto interfaceIdx = CouplingManager::interfaceIdx;
    static constexpr auto darcyIdx = CouplingManager::darcyIdx;

    static constexpr bool adapterUsed = ModelTraits<darcyIdx>::numFluidPhases() > 1;
    using IndexHelper = Dumux::IndexHelper<stokesIdx, interfaceIdx, darcyIdx, FluidSystem<stokesIdx>, adapterUsed>;

    static constexpr int enableEnergyBalance = GetPropType<SubDomainTypeTag<stokesIdx>, Properties::ModelTraits>::enableEnergyBalance();
    static_assert(GetPropType<SubDomainTypeTag<darcyIdx>, Properties::ModelTraits>::enableEnergyBalance() == enableEnergyBalance
               && GetPropType<SubDomainTypeTag<interfaceIdx>, Properties::ModelTraits>::enableEnergyBalance() == enableEnergyBalance,
                  "All submodels must both be either isothermal or non-isothermal");

    static_assert(IsSameFluidSystem<FluidSystem<stokesIdx>, FluidSystem<darcyIdx>>::value, "All submodels must use the same fluid system");
    static_assert(IsSameFluidSystem<FluidSystem<stokesIdx>, FluidSystem<interfaceIdx>>::value, "All submodels must use the same fluid system");

    using DiffusionCoefficientAveragingType = typename StokesDarcyCouplingOptions::DiffusionCoefficientAveragingType;

public:
    StokesDropsDarcyCouplingDataImplementationBase(const CouplingManager& couplingmanager): couplingManager_(couplingmanager) {}

    /*!
     * \brief Returns the corresponding phase index needed for coupling.
     */
    template<std::size_t i>
    static constexpr auto couplingPhaseIdx(Dune::index_constant<i> id, int coupledPhaseIdx = 0)
    { return IndexHelper::couplingPhaseIdx(id, coupledPhaseIdx); }

    /*!
     * \brief Returns the corresponding component index needed for coupling.
     */
    template<std::size_t i>
    static constexpr auto couplingCompIdx(Dune::index_constant<i> id, int coupledCompdIdx)
    { return IndexHelper::couplingCompIdx(id, coupledCompdIdx); }

    /*!
     * \brief Returns a reference to the coupling manager.
     */
    const CouplingManager& couplingManager() const
    { return couplingManager_; }

    /*!
     * \brief Returns the intrinsic permeability of the coupled interface element.
     *
     * Needed for BJS condition.
     */
    Scalar darcyPermeability(const Element<stokesIdx>& element, const SubControlVolumeFace<stokesIdx>& scvf) const
    {
        // TODO only implemented for homogeneous permeability
        return couplingManager().problem(darcyIdx).spatialParams().permeabilityAtPos(scvf.center());
    }

    /*!
     * \brief Returns the momentum flux across the coupling boundary.
     *
     * For the normal momentum coupling, the porous medium side of the coupling condition
     * is evaluated, i.e. -[p n]^pm.
     *
     */
    template<class ElementFaceVariables>
    Scalar momentumCouplingCondition(const Element<stokesIdx>& element,
                                     const FVElementGeometry<stokesIdx>& fvGeometry,
                                     const ElementVolumeVariables<stokesIdx>& stokesElemVolVars,
                                     const ElementFaceVariables& stokesElemFaceVars,
                                     const SubControlVolumeFace<stokesIdx>& scvf) const
    {
        Scalar momentumFlux(0.0);
        const auto& stokesContext = couplingManager_.stokesCouplingContext(element, scvf);
        const auto interfacePhaseIdx = couplingPhaseIdx(interfaceIdx);

        momentumFlux = stokesContext.volVars.pressure(interfacePhaseIdx);

        // normalize pressure
        if(getPropValue<SubDomainTypeTag<stokesIdx>, Properties::NormalizePressure>())
            momentumFlux -= couplingManager_.problem(stokesIdx).initial(scvf)[Indices<stokesIdx>::pressureIdx];

        momentumFlux *= scvf.directionSign();

        return momentumFlux;
    }

    /*!
     * \brief Evaluate an advective flux across the interface and consider upwinding.
     */
    Scalar advectiveFlux(const Scalar insideQuantity, const Scalar outsideQuantity, const Scalar volumeFlow, bool insideIsUpstream) const
    {
        const Scalar upwindWeight = 1.0;

        if(insideIsUpstream)
            return (upwindWeight * insideQuantity + (1.0 - upwindWeight) * outsideQuantity) * volumeFlow;
        else
            return (upwindWeight * outsideQuantity + (1.0 - upwindWeight) * insideQuantity) * volumeFlow;
    }

protected:

/*!
 * \brief Returns the transmissibility used for either molecular diffusion or thermal conductivity.
 */
template<std::size_t i, std::size_t j>
Scalar transmissibility_(Dune::index_constant<i> domainI,
                         Dune::index_constant<j> domainJ,
                         const Scalar insideDistance,
                         const Scalar outsideDistance,
                         const Scalar avgQuantityI,
                         const Scalar avgQuantityJ,
                         const DiffusionCoefficientAveragingType diffCoeffAvgType) const
{
    const Scalar totalDistance = insideDistance + outsideDistance;
    if(diffCoeffAvgType == DiffusionCoefficientAveragingType::harmonic)
    {
        return harmonicMean(avgQuantityI, avgQuantityJ, insideDistance, outsideDistance)
               / totalDistance;
    }
    else if(diffCoeffAvgType == DiffusionCoefficientAveragingType::arithmethic)
    {
        return arithmeticMean(avgQuantityI, avgQuantityJ, insideDistance, outsideDistance)
               / totalDistance;
    }
    else if(diffCoeffAvgType == DiffusionCoefficientAveragingType::ffOnly)
        return domainI == stokesIdx
                        ? avgQuantityI / totalDistance
                        : avgQuantityJ / totalDistance;

    else // diffCoeffAvgType == DiffusionCoefficientAveragingType::pmOnly)
        return domainI == darcyIdx
                        ? avgQuantityI / totalDistance
                        : avgQuantityJ / totalDistance;
}

/*!
 * \brief Returns the distance between an scvf and the corresponding scv center.
 */
template<class Scv>
Scalar getDistance_(const Scv& scv, const GlobalPosition& globalPos) const
{
    return (scv.dofPosition() - globalPos).two_norm();
}

/*!
 * \brief Returns the conductive energy flux acorss the interface.
 */
template<std::size_t i, std::size_t j, bool isNI = enableEnergyBalance, typename std::enable_if_t<isNI, int> = 0>
Scalar conductiveEnergyFlux_(Dune::index_constant<i> domainI,
                             Dune::index_constant<j> domainJ,
                             const FVElementGeometry<i>& fvGeometryI,
                             const FVElementGeometry<j>& fvGeometryJ,
                             const GlobalPosition center,
                             const SubControlVolume<i>& scvI,
                             const SubControlVolume<j>& scvJ,
                             const VolumeVariables<i>& volVarsI,
                             const VolumeVariables<j>& volVarsJ,
                             const DiffusionCoefficientAveragingType diffCoeffAvgType) const
{
    const Scalar insideDistance = getDistance_(scvI, center);
    const Scalar outsideDistance = getDistance_(scvJ, center);

    const Scalar deltaT = volVarsJ.temperature() - volVarsI.temperature();
    const Scalar tij = transmissibility_(domainI,
                                         domainJ,
                                         insideDistance,
                                         outsideDistance,
                                         thermalConductivity_(volVarsI, fvGeometryI, scvI),
                                         thermalConductivity_(volVarsJ, fvGeometryJ, scvJ),
                                         diffCoeffAvgType);

    return -tij * deltaT;
}

/*!
 * \brief Returns the thermal conductivity of the fluid phase within the free flow domain.
 */
template<bool isNI = enableEnergyBalance, typename std::enable_if_t<isNI, int> = 0>
Scalar thermalConductivity_(const VolumeVariables<stokesIdx>& volVars,
                            const FVElementGeometry<stokesIdx>& fvGeometry,
                            const SubControlVolume<stokesIdx>& scv) const
{
    return  volVars.effectiveThermalConductivity();
}

/*!
 * \brief Returns the effective thermal conductivity (lumped parameter) within the interface.
 */
template<bool isNI = enableEnergyBalance, typename std::enable_if_t<isNI, int> = 0>
Scalar thermalConductivity_(const VolumeVariables<interfaceIdx>& volVars,
                            const FVElementGeometry<interfaceIdx>& fvGeometry,
                            const SubControlVolume<interfaceIdx>& scv) const
{
    using ThermalConductivityModel = GetPropType<SubDomainTypeTag<interfaceIdx>, Properties::ThermalConductivityModel>;
    const auto& problem = this->couplingManager().problem(interfaceIdx);
    return ThermalConductivityModel::effectiveThermalConductivity(volVars, problem.spatialParams(), fvGeometry.fvGridGeometry().element(scv), fvGeometry, scv);
}

/*!
 * \brief Returns the effective thermal conductivity (lumped parameter) within the porous medium.
 */
template<bool isNI = enableEnergyBalance, typename std::enable_if_t<isNI, int> = 0>
Scalar thermalConductivity_(const VolumeVariables<darcyIdx>& volVars,
                            const FVElementGeometry<darcyIdx>& fvGeometry,
                            const SubControlVolume<darcyIdx>& scv) const
{
    using ThermalConductivityModel = GetPropType<SubDomainTypeTag<darcyIdx>, Properties::ThermalConductivityModel>;
    const auto& problem = this->couplingManager().problem(darcyIdx);
    return ThermalConductivityModel::effectiveThermalConductivity(volVars, problem.spatialParams(), fvGeometry.fvGridGeometry().element(scv), fvGeometry, scv);
}

private:
    const CouplingManager& couplingManager_;
};

/*!
 * \ingroup StokesDarcyCoupling
 * \brief Coupling data specialization for compositional models.
 */
template<class MDTraits, class CouplingManager, bool enableEnergyBalance>
class StokesDropsDarcyCouplingDataImplementation<MDTraits, CouplingManager, enableEnergyBalance, true>
: public StokesDropsDarcyCouplingDataImplementationBase<MDTraits, CouplingManager>
{
    using ParentType = StokesDropsDarcyCouplingDataImplementationBase<MDTraits, CouplingManager>;
    using Scalar = typename MDTraits::Scalar;
    static constexpr auto stokesIdx = typename MDTraits::template SubDomain<0>::Index();
    static constexpr auto stokesCellCenterIdx = stokesIdx;
    static constexpr auto stokesFaceIdx = typename MDTraits::template SubDomain<1>::Index();
    static constexpr auto interfaceIdx = typename MDTraits::template SubDomain<2>::Index();
    static constexpr auto darcyIdx = typename MDTraits::template SubDomain<3>::Index();

    static constexpr int interfaceNormalIdx = 1; // interface normal to x-direction, parallel to y-direction

    // the sub domain type tags
    template<std::size_t id>
    using SubDomainTypeTag = typename MDTraits::template SubDomain<id>::TypeTag;

    template<std::size_t id> using FVGridGeometry = GetPropType<SubDomainTypeTag<id>, Properties::FVGridGeometry>;
    template<std::size_t id> using Element = typename FVGridGeometry<id>::GridView::template Codim<0>::Entity;
    template<std::size_t id> using FVElementGeometry = typename FVGridGeometry<id>::LocalView;
    template<std::size_t id> using SubControlVolumeFace = typename FVElementGeometry<id>::SubControlVolumeFace;
    template<std::size_t id> using SubControlVolume = typename FVGridGeometry<id>::LocalView::SubControlVolume;
    template<std::size_t id> using Indices = typename GetPropType<SubDomainTypeTag<id>, Properties::ModelTraits>::Indices;
    template<std::size_t id> using ElementVolumeVariables = typename GetPropType<SubDomainTypeTag<id>, Properties::GridVolumeVariables>::LocalView;
    template<std::size_t id> using ElementFaceVariables = typename GetPropType<SubDomainTypeTag<id>, Properties::GridFaceVariables>::LocalView;
    template<std::size_t id> using VolumeVariables  = typename GetPropType<SubDomainTypeTag<id>, Properties::GridVolumeVariables>::VolumeVariables;
    template<std::size_t id> using FluidSystem  = GetPropType<SubDomainTypeTag<id>, Properties::FluidSystem>;

    using GlobalPosition = typename Element<0>::Geometry::GlobalCoordinate;

    static constexpr auto numComponents = GetPropType<SubDomainTypeTag<stokesIdx>, Properties::ModelTraits>::numFluidComponents();
    static constexpr auto replaceCompEqIdx = GetPropType<SubDomainTypeTag<stokesIdx>, Properties::ModelTraits>::replaceCompEqIdx();
    static constexpr bool useMoles = GetPropType<SubDomainTypeTag<stokesIdx>, Properties::ModelTraits>::useMoles();
    static constexpr auto replaceCompEqIdxIF = GetPropType<SubDomainTypeTag<interfaceIdx>, Properties::ModelTraits>::replaceCompEqIdx();
    static constexpr auto replaceCompEqIdxPM = GetPropType<SubDomainTypeTag<darcyIdx>, Properties::ModelTraits>::replaceCompEqIdx();

    static_assert(GetPropType<SubDomainTypeTag<interfaceIdx>, Properties::ModelTraits>::numFluidComponents() == numComponents, "FF and IF models must use same number of components");
    static_assert(getPropValue<SubDomainTypeTag<interfaceIdx>, Properties::UseMoles>() == useMoles, "FF and IF models must either use moles or not");
    static_assert(getPropValue<SubDomainTypeTag<interfaceIdx>, Properties::ReplaceCompEqIdx>() == replaceCompEqIdx, "FF and IF models must use the same replaceCompEqIdx");

    static_assert(GetPropType<SubDomainTypeTag<darcyIdx>, Properties::ModelTraits>::numFluidComponents() == numComponents, "FF and PM models must use same number of components");
    static_assert(getPropValue<SubDomainTypeTag<darcyIdx>, Properties::UseMoles>() == useMoles, "FF and PM models must either use moles or not");
    static_assert(getPropValue<SubDomainTypeTag<darcyIdx>, Properties::ReplaceCompEqIdx>() == replaceCompEqIdx, "FF and PM models must use the same replaceCompEqIdx");

    using NumEqVector = Dune::FieldVector<Scalar, numComponents>;

    using DiffusionCoefficientAveragingType = typename StokesDarcyCouplingOptions::DiffusionCoefficientAveragingType;

    static constexpr bool isFicksLaw = IsFicksLaw<typename GET_PROP_TYPE(SubDomainTypeTag<stokesIdx>, MolecularDiffusionType)>();
    static_assert(isFicksLaw == IsFicksLaw<typename GET_PROP_TYPE(SubDomainTypeTag<interfaceIdx>, MolecularDiffusionType)>(),
                  "Both submodels must use the same diffusion law.");
    static_assert(isFicksLaw == IsFicksLaw<typename GET_PROP_TYPE(SubDomainTypeTag<darcyIdx>, MolecularDiffusionType)>(),
                  "Both submodels must use the same diffusion law.");

public:
    using ParentType::ParentType;
    using ParentType::couplingPhaseIdx;
    using ParentType::couplingCompIdx;

    /*!
     * \brief Returns the mass flux across the coupling boundary as seen from the free-flow domain.
     */
    NumEqVector massCouplingCondition(const Element<stokesIdx>& element,
                                      const FVElementGeometry<stokesIdx>& fvGeometry,
                                      const ElementVolumeVariables<stokesIdx>& stokesElemVolVars,
                                      const ElementFaceVariables<stokesIdx>& stokesElemFaceVars,
                                      const SubControlVolumeFace<stokesIdx>& scvf,
                                      const DiffusionCoefficientAveragingType diffCoeffAvgType = DiffusionCoefficientAveragingType::harmonic) const
    {
        const auto& stokesContext = this->couplingManager().stokesCouplingContext(element, scvf);
        const Scalar velocity = stokesElemFaceVars[scvf].velocitySelf();
        const auto& stokesVolVars = stokesElemVolVars[scvf.insideScvIdx()];
        const auto& interfaceVolVars = stokesContext.volVars;
        const auto& outsideScv = (*scvs(stokesContext.fvGeometry).begin());
        const bool insideIsUpstream = sign(velocity) == scvf.directionSign();

        NumEqVector massFlux = massFlux_(stokesIdx, interfaceIdx, fvGeometry, scvf.center(),
                                        stokesVolVars, interfaceVolVars, outsideScv,
                                        couplingPhaseIdx(stokesIdx), couplingPhaseIdx(interfaceIdx),
                                        velocity * scvf.directionSign(), insideIsUpstream, diffCoeffAvgType);

        return massFlux;
    }

    /*!
     * \brief Returns the mass flux across the coupling boundary as seen from the interface domain.
     */
    NumEqVector massCouplingCondition(const Element<interfaceIdx>& element,
                                      const FVElementGeometry<interfaceIdx>& fvGeometry,
                                      const ElementVolumeVariables<interfaceIdx>& interfaceElemVolVars,
                                      const SubControlVolume<interfaceIdx>& scv,
                                      const DiffusionCoefficientAveragingType diffCoeffAvgType = DiffusionCoefficientAveragingType::harmonic) const
    {
        const auto& interfaceContext = this->couplingManager().interfaceCouplingContext(element, scv);

        // flux from free flow
        const auto& interfaceVolVars = interfaceElemVolVars[scv.dofIndex()];
        const auto& stokesVolVars = interfaceContext.stokesVolVars;
        const Scalar topVelocity = interfaceContext.stokesVelocity[interfaceNormalIdx];
        const bool topInsideIsUpstream = topVelocity > 0.0;
        const auto& outsideScv = (*scvs(interfaceContext.stokesFvGeometry).begin());

        GlobalPosition center {scv.center(), interfaceContext.interfaceYCoordinate};
        NumEqVector massFluxFF = massFlux_(interfaceIdx, stokesIdx, fvGeometry, center,
                                interfaceVolVars, stokesVolVars, outsideScv,
                                couplingPhaseIdx(interfaceIdx), couplingPhaseIdx(stokesIdx),
                                topVelocity, topInsideIsUpstream, diffCoeffAvgType);

        // flux from porous medium
        const auto& darcyVolVars = interfaceContext.darcyVolVars;

        const Scalar distanceToDarcy = std::abs(interfaceContext.interfaceYCoordinate - interfaceContext.darcyElement.geometry().center()[interfaceNormalIdx]);
        const Scalar gravity = this->couplingManager().problem(interfaceIdx).gravity()[interfaceNormalIdx];

        // gas flux (aGas)
        // compute velocity at the interface (pressure gradient between interface and porous medium)
        const Scalar diffPGas = darcyVolVars.pressure(couplingPhaseIdx(darcyIdx)) - interfaceElemVolVars[scv.dofIndex()].pressure(couplingPhaseIdx(interfaceIdx));
        const Scalar interfaceDensityGas = useMoles ? interfaceElemVolVars[scv.dofIndex()].molarDensity(couplingPhaseIdx(interfaceIdx))
                                                    : interfaceElemVolVars[scv.dofIndex()].density(couplingPhaseIdx(interfaceIdx));
        const Scalar darcyDensityGas = useMoles ? darcyVolVars.molarDensity(couplingPhaseIdx(darcyIdx))
                                                : darcyVolVars.density(couplingPhaseIdx(darcyIdx));
        Scalar densityGas = diffPGas < 0 ? interfaceDensityGas : darcyDensityGas;
        const Scalar permeability = darcyVolVars.permeability();
        Scalar mobility = darcyVolVars.mobility(couplingPhaseIdx(darcyIdx));

        const Scalar bottomVelocityGas = -1.0 * mobility * permeability * (diffPGas/distanceToDarcy + gravity * densityGas);
        const bool bottomInsideIsUpstreamGas = bottomVelocityGas > 0.0;

        // liquid flux (aDrop)
        // compute velocity at the interface (pressure gradient between interface and porous medium)
        const Scalar diffPLiquid = darcyVolVars.pressure(1-couplingPhaseIdx(darcyIdx)) - interfaceElemVolVars[scv.dofIndex()].pressure(1-couplingPhaseIdx(interfaceIdx));

        const Scalar interfaceDensityLiquid = useMoles ? interfaceElemVolVars[scv.dofIndex()].molarDensity(1-couplingPhaseIdx(interfaceIdx))
                                                       : interfaceElemVolVars[scv.dofIndex()].density(1-couplingPhaseIdx(interfaceIdx));
        const Scalar darcyDensityLiquid = useMoles ? darcyVolVars.molarDensity(1-couplingPhaseIdx(darcyIdx))
                                                   : darcyVolVars.density(1-couplingPhaseIdx(darcyIdx));
        const Scalar densityLiquid = diffPLiquid < 0 ? interfaceDensityLiquid : darcyDensityLiquid;

        mobility = darcyVolVars.mobility(1-couplingPhaseIdx(darcyIdx));

        const Scalar bottomVelocityLiquid = -1.0 * mobility * permeability * (diffPLiquid/distanceToDarcy + gravity * densityLiquid);
        const bool bottomInsideIsUpstreamLiquid = bottomVelocityLiquid > 0.0;

        // flux from porous medium
        NumEqVector massFluxGasPM = massFlux_(interfaceIdx, darcyIdx, fvGeometry, center,
                                              interfaceVolVars, darcyVolVars, outsideScv,
                                              couplingPhaseIdx(interfaceIdx), couplingPhaseIdx(darcyIdx),
                                              bottomVelocityGas, bottomInsideIsUpstreamGas, diffCoeffAvgType);
        NumEqVector massFluxLiquidPM = massFlux_(interfaceIdx, darcyIdx, fvGeometry, center,
                                                interfaceVolVars, darcyVolVars, outsideScv,
                                                1-couplingPhaseIdx(interfaceIdx), 1-couplingPhaseIdx(darcyIdx),
                                                bottomVelocityLiquid, bottomInsideIsUpstreamLiquid, diffCoeffAvgType);

        // scale with available area fractions
        const auto interfaceElemIdx = interfaceContext.interfaceElementIdx;
        const Scalar aLiquid = this->couplingManager().dropManager().aLiquid(interfaceElemIdx);
        const Scalar aGas = this->couplingManager().dropManager().aGas(interfaceElemIdx);

        massFluxGasPM[0] *= aGas;
        massFluxGasPM[1] *= aGas;
        massFluxLiquidPM[0] *= aLiquid;
        massFluxLiquidPM[1] *= aLiquid;

        // store current water flux for drop formation or growth in next time step
        this->couplingManager().dropManager().setCurrentWaterFlux(interfaceElemIdx, -1.0 * (massFluxLiquidPM[0] + massFluxLiquidPM[1]));

#if NONISOTHERMAL
        const Scalar aSurface = this->couplingManager().dropManager().aSurface(interfaceElemIdx);
        if (aSurface > 0.0)
            this->couplingManager().dropManager().setCurrentEvapFlux(interfaceElemIdx, -1.0 * (massFluxFF[0] + massFluxGasPM[0])/aSurface);
#endif
        return (massFluxGasPM + massFluxLiquidPM + massFluxFF);
    }

    /*!
     * \brief Returns the mass flux across the coupling boundary as seen from the Darcy domain.
     */
    NumEqVector massCouplingCondition(const Element<darcyIdx>& element,
                                      const FVElementGeometry<darcyIdx>& fvGeometry,
                                      const ElementVolumeVariables<darcyIdx>& darcyElemVolVars,
                                      const SubControlVolumeFace<darcyIdx>& scvf,
                                      const DiffusionCoefficientAveragingType diffCoeffAvgType = DiffusionCoefficientAveragingType::harmonic) const
    {
        const auto& darcyContext = this->couplingManager().darcyCouplingContext(element, scvf);
        const auto& darcyVolVars = darcyElemVolVars[scvf.insideScvIdx()];
        const auto& interfaceVolVars = darcyContext.volVars;
        const auto& outsideScv = (*scvs(darcyContext.fvGeometry).begin());
        const auto& insideScv = fvGeometry.scv(scvf.insideScvIdx());

        const Scalar distance = (scvf.center() - insideScv.center()).two_norm();
        const Scalar gravity = this->couplingManager().problem(darcyIdx).gravity()[interfaceNormalIdx];

        // gas flux (aGas)
        // compute velocity at the interface (pressure gradient between interface and porous medium)
        const Scalar diffPGas = interfaceVolVars.pressure(couplingPhaseIdx(interfaceIdx)) - darcyVolVars.pressure(couplingPhaseIdx(darcyIdx));
        const Scalar darcyDensityGas = useMoles ? darcyVolVars.molarDensity(couplingPhaseIdx(darcyIdx))
                                                : darcyVolVars.density(couplingPhaseIdx(darcyIdx));
        const Scalar interfaceDensityGas = useMoles ? interfaceVolVars.molarDensity(couplingPhaseIdx(interfaceIdx))
                                                    : interfaceVolVars.density(couplingPhaseIdx(interfaceIdx));
        Scalar density = diffPGas > 0 ? interfaceDensityGas : darcyDensityGas;
        Scalar permeability = darcyVolVars.permeability();
        Scalar mobility = darcyVolVars.mobility(couplingPhaseIdx(darcyIdx));

        const Scalar velocityGas = -1.0 * permeability * mobility * (diffPGas/distance - gravity * density);
        const bool insideIsUpstreamGas = velocityGas < 0.0;

        NumEqVector massFluxGas = massFlux_(darcyIdx, interfaceIdx, fvGeometry, scvf.center(),
                                  darcyVolVars, interfaceVolVars, outsideScv,
                                  couplingPhaseIdx(darcyIdx), couplingPhaseIdx(interfaceIdx),
                                  velocityGas, insideIsUpstreamGas, diffCoeffAvgType);
        // liquid flux (aDrop)
        // compute velocity at the interface (pressure gradient between interface and porous medium)
        const Scalar diffPLiquid = interfaceVolVars.pressure(1-couplingPhaseIdx(interfaceIdx)) - darcyVolVars.pressure(1-couplingPhaseIdx(darcyIdx));
        const Scalar darcyDensityLiquid = useMoles ? darcyVolVars.molarDensity(1-couplingPhaseIdx(darcyIdx))
                                                   : darcyVolVars.density(1-couplingPhaseIdx(darcyIdx));
        const Scalar interfaceDensityLiquid = useMoles ? interfaceVolVars.molarDensity(1-couplingPhaseIdx(interfaceIdx))
                                                       : interfaceVolVars.density(1-couplingPhaseIdx(interfaceIdx));
        density = diffPLiquid > 0 ? interfaceDensityLiquid : darcyDensityLiquid;
        permeability = darcyVolVars.permeability();
        mobility = darcyVolVars.mobility(1-couplingPhaseIdx(darcyIdx));

        const Scalar velocityLiquid = -1.0 * permeability * mobility * (diffPLiquid/distance - gravity * density);
        const bool insideIsUpstreamLiquid = velocityLiquid < 0.0;

        NumEqVector massFluxLiquid = massFlux_(darcyIdx, interfaceIdx, fvGeometry, scvf.center(),
                                     darcyVolVars, interfaceVolVars, outsideScv,
                                     1-couplingPhaseIdx(darcyIdx), 1-couplingPhaseIdx(interfaceIdx),
                                     velocityLiquid, insideIsUpstreamLiquid, diffCoeffAvgType);

        // scale with available area fractions
        const auto interfaceElemIdx = darcyContext.interfaceElementIdx;
        const Scalar aLiquid = this->couplingManager().dropManager().aLiquid(interfaceElemIdx);
        const Scalar aGas = this->couplingManager().dropManager().aGas(interfaceElemIdx);

        massFluxGas[0] *= aGas;
        massFluxGas[1] *= aGas;
        massFluxLiquid[0] *= aLiquid;
        massFluxLiquid[1] *= aLiquid;

        return massFluxGas + massFluxLiquid;
    }

/*!
 * \brief Returns the energy flux across the coupling boundary as seen from the free-flow domain.
 */
template<bool isNI = enableEnergyBalance, typename std::enable_if_t<isNI, int> = 0>
Scalar energyCouplingCondition(const Element<stokesIdx>& element,
                               const FVElementGeometry<stokesIdx>& fvGeometry,
                               const ElementVolumeVariables<stokesIdx>& stokesElemVolVars,
                               const ElementFaceVariables<stokesIdx>& stokesElemFaceVars,
                               const SubControlVolumeFace<stokesIdx>& scvf,
                               const DiffusionCoefficientAveragingType diffCoeffAvgType = DiffusionCoefficientAveragingType::harmonic) const
{
    const auto& stokesContext = this->couplingManager().stokesCouplingContext(element, scvf);
    const auto& stokesVolVars = stokesElemVolVars[scvf.insideScvIdx()];
    const auto& interfaceVolVars = stokesContext.volVars;

    const Scalar velocity = stokesElemFaceVars[scvf].velocitySelf();
    const bool insideIsUpstream = sign(velocity) == scvf.directionSign();

    return energyFlux_(stokesIdx, interfaceIdx, fvGeometry, stokesContext.fvGeometry, scvf.center(),
                                                           stokesVolVars, interfaceVolVars,
                                                           couplingPhaseIdx(stokesIdx), couplingPhaseIdx(interfaceIdx),
                                                           velocity * scvf.directionSign(), insideIsUpstream, diffCoeffAvgType);
}

/*!
 * \brief Returns the energy flux across the coupling boundary as seen from the interface domain.
 */
template<bool isNI = enableEnergyBalance, typename std::enable_if_t<isNI, int> = 0>
Scalar energyCouplingCondition(const Element<interfaceIdx>& element,
                               const FVElementGeometry<interfaceIdx>& fvGeometry,
                               const ElementVolumeVariables<interfaceIdx>& interfaceElemVolVars,
                               const SubControlVolume<interfaceIdx>& scv,
                               const DiffusionCoefficientAveragingType diffCoeffAvgType = DiffusionCoefficientAveragingType::harmonic) const
{
    const auto& interfaceContext = this->couplingManager().interfaceCouplingContext(element, scv);
    GlobalPosition center {scv.center(), interfaceContext.interfaceYCoordinate};

    const auto& interfaceVolVars = interfaceElemVolVars[scv.dofIndex()];
    const auto& stokesVolVars = interfaceContext.stokesVolVars;

    const Scalar topVelocity = interfaceContext.stokesVelocity[interfaceNormalIdx];
    const bool topInsideIsUpstream = topVelocity > 0.0;

    const auto& darcyVolVars = interfaceContext.darcyVolVars;
    const Scalar distanceToDarcy = std::abs(interfaceContext.interfaceYCoordinate - interfaceContext.darcyElement.geometry().center()[interfaceNormalIdx]);
    const Scalar gravity = this->couplingManager().problem(interfaceIdx).gravity()[interfaceNormalIdx];

    // gas flux (aGas)
    // compute velocity at the interface (pressure gradient between interface and porous medium)
    const Scalar diffPGas = darcyVolVars.pressure(couplingPhaseIdx(darcyIdx)) - interfaceElemVolVars[scv.dofIndex()].pressure(couplingPhaseIdx(interfaceIdx));
    const Scalar interfaceDensityGas = interfaceElemVolVars[scv.dofIndex()].density(couplingPhaseIdx(interfaceIdx));
    const Scalar darcyDensityGas = interfaceContext.darcyVolVars.density(couplingPhaseIdx(darcyIdx));
    Scalar densityGas = diffPGas < 0 ? interfaceDensityGas : darcyDensityGas;
    Scalar permeability = darcyVolVars.permeability();
    Scalar mobility = interfaceContext.darcyVolVars.mobility(couplingPhaseIdx(darcyIdx));

    const Scalar bottomVelocityGas = -1.0 * mobility * permeability * (diffPGas/distanceToDarcy + gravity * densityGas);
    const bool bottomInsideIsUpstreamGas = bottomVelocityGas > 0.0;

    // liquid flux (aDrop)
    // compute velocity at the interface (pressure gradient between interface and porous medium)
    const Scalar diffPLiquid = darcyVolVars.pressure(1-couplingPhaseIdx(darcyIdx)) - interfaceElemVolVars[scv.dofIndex()].pressure(1-couplingPhaseIdx(interfaceIdx));
    const Scalar interfaceDensityLiquid = interfaceElemVolVars[scv.dofIndex()].density(1-couplingPhaseIdx(interfaceIdx));
    const Scalar darcyDensityLiquid = interfaceContext.darcyVolVars.density(1-couplingPhaseIdx(darcyIdx));
    const Scalar densityLiquid = diffPLiquid < 0 ? interfaceDensityLiquid : darcyDensityLiquid;
    mobility = interfaceContext.darcyVolVars.mobility(1-couplingPhaseIdx(darcyIdx));
    const Scalar bottomVelocityLiquid = -1.0 * mobility * permeability * (diffPLiquid/distanceToDarcy + gravity * densityLiquid);
    const bool bottomInsideIsUpstreamLiquid = bottomVelocityLiquid > 0.0;

    Scalar energyFluxFF = energyFlux_(interfaceIdx, stokesIdx, fvGeometry, interfaceContext.stokesFvGeometry, center,
                                    interfaceVolVars, stokesVolVars,
                                    couplingPhaseIdx(interfaceIdx), couplingPhaseIdx(stokesIdx),
                                    topVelocity, topInsideIsUpstream, diffCoeffAvgType);
    Scalar energyFluxGasPM = energyFlux_(interfaceIdx, darcyIdx, fvGeometry, interfaceContext.darcyFvGeometry, center,
                                         interfaceVolVars, darcyVolVars,
                                         couplingPhaseIdx(interfaceIdx), couplingPhaseIdx(darcyIdx),
                                         bottomVelocityGas, bottomInsideIsUpstreamGas, diffCoeffAvgType);

    Scalar energyFluxLiquidPM = energyFlux_(interfaceIdx, darcyIdx, fvGeometry, interfaceContext.darcyFvGeometry, center,
                                            interfaceVolVars, darcyVolVars,
                                            1-couplingPhaseIdx(interfaceIdx), 1-couplingPhaseIdx(darcyIdx),
                                            bottomVelocityLiquid, bottomInsideIsUpstreamLiquid, diffCoeffAvgType);

    // scale with available area fractions
    const int interfaceElementIdx = interfaceContext.interfaceElementIdx;
    const Scalar aLiquid = this->couplingManager().dropManager().aLiquid(interfaceElementIdx);
    const Scalar aGas = this->couplingManager().dropManager().aGas(interfaceElementIdx);

    energyFluxGasPM *= aGas;
    energyFluxLiquidPM *= aLiquid;

    return energyFluxFF + energyFluxGasPM + energyFluxLiquidPM;
}

/*!
 * \brief Returns the energy flux across the coupling boundary as seen from the Darcy domain.
 */
template<bool isNI = enableEnergyBalance, typename std::enable_if_t<isNI, int> = 0>
Scalar energyCouplingCondition(const Element<darcyIdx>& element,
                               const FVElementGeometry<darcyIdx>& fvGeometry,
                               const ElementVolumeVariables<darcyIdx>& darcyElemVolVars,
                               const SubControlVolumeFace<darcyIdx>& scvf,
                               const DiffusionCoefficientAveragingType diffCoeffAvgType = DiffusionCoefficientAveragingType::harmonic) const
{
    const auto& darcyContext = this->couplingManager().darcyCouplingContext(element, scvf);
    const auto& insideScv = fvGeometry.scv(scvf.insideScvIdx());

    const auto& darcyVolVars = darcyElemVolVars[scvf.insideScvIdx()];
    const auto& interfaceVolVars = darcyContext.volVars;

    const Scalar distance = (scvf.center() - insideScv.center()).two_norm();
    const Scalar gravity = this->couplingManager().problem(darcyIdx).gravity()[interfaceNormalIdx];

    // gas flux (aGas)
    // compute velocity at the interface (pressure gradient between interface and porous medium)
    const Scalar diffPGas = interfaceVolVars.pressure(couplingPhaseIdx(interfaceIdx)) - darcyVolVars.pressure(couplingPhaseIdx(darcyIdx));
    const Scalar darcyDensityGas = darcyVolVars.density(couplingPhaseIdx(darcyIdx));
    const Scalar interfaceDensityGas = interfaceVolVars.density(couplingPhaseIdx(interfaceIdx));

    Scalar density = diffPGas > 0 ? interfaceDensityGas : darcyDensityGas;
    Scalar permeability = darcyVolVars.permeability();
    Scalar mobility = darcyVolVars.mobility(couplingPhaseIdx(darcyIdx));

    const Scalar velocityGas = - 1.0 * permeability * mobility * (diffPGas/distance - gravity * density);
    const bool insideIsUpstreamGas = velocityGas > 0.0;

    Scalar energyFluxGas = energyFlux_(darcyIdx, interfaceIdx, fvGeometry, darcyContext.fvGeometry, scvf.center(),
                                                                                   darcyVolVars, interfaceVolVars,
                                                                                   couplingPhaseIdx(darcyIdx), couplingPhaseIdx(interfaceIdx),
                                                                                   velocityGas, insideIsUpstreamGas, diffCoeffAvgType);
    // liquid flux (aDrop)
    // compute velocity at the interface (pressure gradient between interface and porous medium)
    const Scalar diffPLiquid = interfaceVolVars.pressure(1-couplingPhaseIdx(interfaceIdx)) - darcyVolVars.pressure(1-couplingPhaseIdx(darcyIdx));
    const Scalar darcyDensityLiquid = darcyVolVars.density(1-couplingPhaseIdx(darcyIdx));
    const Scalar interfaceDensityLiquid = interfaceVolVars.density(1-couplingPhaseIdx(interfaceIdx));

    density = diffPLiquid > 0 ? interfaceDensityLiquid : darcyDensityLiquid;
    mobility = darcyVolVars.mobility(1-couplingPhaseIdx(darcyIdx));

    const Scalar velocityLiquid = - 1.0 * permeability * mobility * (diffPLiquid/distance - gravity * density);
    const bool insideIsUpstreamLiquid = velocityLiquid > 0.0;

    Scalar energyFluxLiquid = energyFlux_(darcyIdx, interfaceIdx, fvGeometry, darcyContext.fvGeometry, scvf.center(),
                                                          darcyVolVars, interfaceVolVars,
                                                          1-couplingPhaseIdx(darcyIdx), 1-couplingPhaseIdx(interfaceIdx),
                                                          velocityLiquid, insideIsUpstreamLiquid, diffCoeffAvgType);

    // scale with available area fractions
    const auto interfaceElemIdx = darcyContext.interfaceElementIdx;
    const Scalar aLiquid = this->couplingManager().dropManager().aLiquid(interfaceElemIdx);
    const Scalar aGas = this->couplingManager().dropManager().aGas(interfaceElemIdx);

    energyFluxGas *= aGas;
    energyFluxLiquid *= aLiquid;

    return (energyFluxGas + energyFluxLiquid);
}

protected:
    /*!
     * \brief Evaluate the compositional mole/mass flux across the interface.
     */
     // TODO copied from stokesdarcycoupling/couplingdata --> inherit?
    template<std::size_t i, std::size_t j>
    NumEqVector massFlux_(Dune::index_constant<i> domainI,
                          Dune::index_constant<j> domainJ,
                          const FVElementGeometry<i>& insideFvGeometry,
                          const GlobalPosition center,
                          const VolumeVariables<i>& insideVolVars,
                          const VolumeVariables<j>& outsideVolVars,
                          const SubControlVolume<j>& outsideScv,
                          const int domainIPhaseIdx,
                          const int domainJPhaseIdx,
                          const Scalar velocity,
                          const bool insideIsUpstream,
                          const DiffusionCoefficientAveragingType diffCoeffAvgType) const
    {
        NumEqVector flux(0.0);
        NumEqVector diffusiveFlux(0.0);

        auto moleOrMassFraction = [&](const auto& volVars, int phaseIdx, int compIdx)
        { return useMoles ? volVars.moleFraction(phaseIdx, compIdx) : volVars.massFraction(phaseIdx, compIdx); };

        auto moleOrMassDensity = [&](const auto& volVars, int phaseIdx)
        { return useMoles ? volVars.molarDensity(phaseIdx) : volVars.density(phaseIdx); };

        // treat the advective fluxes
        auto insideTerm = [&](int compIdx)
        { return moleOrMassFraction(insideVolVars, domainIPhaseIdx, compIdx) * moleOrMassDensity(insideVolVars, domainIPhaseIdx); };

        auto outsideTerm = [&](int compIdx)
        { return moleOrMassFraction(outsideVolVars, domainJPhaseIdx, compIdx) * moleOrMassDensity(outsideVolVars, domainJPhaseIdx); };

        for(int compIdx = 0; compIdx < numComponents; ++compIdx)
        {
            const int domainICompIdx = couplingCompIdx(domainI, compIdx);
            const int domainJCompIdx = couplingCompIdx(domainJ, compIdx);
            flux[domainICompIdx] += this->advectiveFlux(insideTerm(domainICompIdx), outsideTerm(domainJCompIdx), velocity, insideIsUpstream);
        }

        // treat the diffusive fluxes
        const auto& insideScv = (*scvs(insideFvGeometry).begin());

        diffusiveFlux += diffusiveMolecularFluxFicksLaw_(domainI, domainJ, center, domainIPhaseIdx, domainJPhaseIdx, insideScv, outsideScv, insideVolVars, outsideVolVars, diffCoeffAvgType);
        if(!useMoles)
        {
            // convert everything to a mass flux
            for(int compIdx = 0; compIdx < numComponents; ++compIdx)
                {
                    const int domainICompIdx = couplingCompIdx(domainI, compIdx);
                    diffusiveFlux[domainICompIdx] *= FluidSystem<i>::molarMass(domainICompIdx);
                }
        }

        flux += diffusiveFlux;
        // convert to total mass/mole balance, if set by user
        if(replaceCompEqIdx < numComponents)
            flux[replaceCompEqIdx] = std::accumulate(flux.begin(), flux.end(), 0.0);

        return flux;
    }

    /*!
     * \brief Returns the molecular diffusion coefficient within the free flow domain.
     */
    Scalar diffusionCoefficient_(const VolumeVariables<stokesIdx>& volVars, int phaseIdx, int compIdx) const
    {
         return volVars.effectiveDiffusivity(phaseIdx, compIdx);
    }

    /*!
     * \brief Returns the effective diffusion coefficient within the interface domain.
     */
    Scalar diffusionCoefficient_(const VolumeVariables<interfaceIdx>& volVars, int phaseIdx, int compIdx) const
    {
        using EffDiffModel = GetPropType<SubDomainTypeTag<interfaceIdx>, Properties::EffectiveDiffusivityModel>;
        return EffDiffModel::effectiveDiffusivity(volVars.porosity(), // TODO
                                                  volVars.saturation(phaseIdx),
                                                  volVars.diffusionCoefficient(phaseIdx, compIdx));
    }

    /*!
     * \brief Returns the effective diffusion coefficient within the porous medium.
     */
    Scalar diffusionCoefficient_(const VolumeVariables<darcyIdx>& volVars, int phaseIdx, int compIdx) const
    {
        using EffDiffModel = GetPropType<SubDomainTypeTag<darcyIdx>, Properties::EffectiveDiffusivityModel>;
        return EffDiffModel::effectiveDiffusivity(volVars.porosity(),
                                                  volVars.saturation(phaseIdx),
                                                  volVars.diffusionCoefficient(phaseIdx, compIdx));
    }


template<std::size_t i, std::size_t j>
NumEqVector diffusiveMolecularFluxFicksLaw_(Dune::index_constant<i> domainI,
                                            Dune::index_constant<j> domainJ,
                                            const GlobalPosition center,
                                            const int domainIPhaseIdx,
                                            const int domainJPhaseIdx,
                                            const SubControlVolume<i>& scvI,
                                            const SubControlVolume<j>& scvJ,
                                            const VolumeVariables<i>& volVarsI,
                                            const VolumeVariables<j>& volVarsJ,
                                            const DiffusionCoefficientAveragingType diffCoeffAvgType) const
{
    NumEqVector diffusiveFlux(0.0);

    const Scalar avgMolarDensity = 0.5 * volVarsI.molarDensity(domainIPhaseIdx) + 0.5 *  volVarsJ.molarDensity(domainJPhaseIdx);

    const Scalar insideDistance = this->getDistance_(scvI, center);
    const Scalar outsideDistance = this->getDistance_(scvJ, center);

    // only valid for 2p2c
    const int domainICompIdx = 1 - domainIPhaseIdx;
    const int domainJCompIdx = 1 - domainJPhaseIdx;

    assert(FluidSystem<i>::componentName(domainICompIdx) == FluidSystem<j>::componentName(domainJCompIdx));

    const Scalar deltaMoleFrac = volVarsJ.moleFraction(domainJPhaseIdx, domainJCompIdx) - volVarsI.moleFraction(domainIPhaseIdx, domainICompIdx);
    const Scalar tij = this->transmissibility_(domainI,
                                               domainJ,
                                               insideDistance,
                                               outsideDistance,
                                               diffusionCoefficient_(volVarsI, domainIPhaseIdx, domainICompIdx),
                                               diffusionCoefficient_(volVarsJ, domainJPhaseIdx, domainJCompIdx),
                                               diffCoeffAvgType);
    diffusiveFlux[domainICompIdx] += -avgMolarDensity * tij * deltaMoleFrac;

    const Scalar cumulativeFlux = std::accumulate(diffusiveFlux.begin(), diffusiveFlux.end(), 0.0);
    diffusiveFlux[couplingCompIdx(domainI, 0)] = -cumulativeFlux;

    return diffusiveFlux;
}

// TODO copied from stokesdarcycoupling/couplingdata
Scalar getComponentEnthalpy(const VolumeVariables<stokesIdx>& volVars, int phaseIdx, int compIdx) const
{
    return FluidSystem<stokesIdx>::componentEnthalpy(volVars.fluidState(), 0, compIdx);
}

Scalar getComponentEnthalpy(const VolumeVariables<interfaceIdx>& volVars, int phaseIdx, int compIdx) const
{
    return FluidSystem<interfaceIdx>::componentEnthalpy(volVars.fluidState(), phaseIdx, compIdx);
}

Scalar getComponentEnthalpy(const VolumeVariables<darcyIdx>& volVars, int phaseIdx, int compIdx) const
{
    return FluidSystem<darcyIdx>::componentEnthalpy(volVars.fluidState(), phaseIdx, compIdx);
}

/*!
 * \brief Evaluate the energy flux across the interface.
 */
template<std::size_t i, std::size_t j, bool isNI = enableEnergyBalance, typename std::enable_if_t<isNI, int> = 0>
Scalar energyFlux_(Dune::index_constant<i> domainI,
                   Dune::index_constant<j> domainJ,
                   const FVElementGeometry<i>& insideFvGeometry,
                   const FVElementGeometry<j>& outsideFvGeometry,
                   const GlobalPosition center,
                   const VolumeVariables<i>& insideVolVars,
                   const VolumeVariables<j>& outsideVolVars,
                   const int domainIPhaseIdx,
                   const int domainJPhaseIdx,
                   const Scalar velocity,
                   const bool insideIsUpstream,
                   const DiffusionCoefficientAveragingType diffCoeffAvgType) const
{
    Scalar flux(0.0);

    const auto& insideScv = (*scvs(insideFvGeometry).begin());
    const auto& outsideScv = (*scvs(outsideFvGeometry).begin());

    // convective fluxes
    const Scalar insideTerm = insideVolVars.density(domainIPhaseIdx) * insideVolVars.enthalpy(domainIPhaseIdx);
    const Scalar outsideTerm = outsideVolVars.density(domainJPhaseIdx) * outsideVolVars.enthalpy(domainJPhaseIdx);

    flux += this->advectiveFlux(insideTerm, outsideTerm, velocity, insideIsUpstream);

    flux += this->conductiveEnergyFlux_(domainI, domainJ, insideFvGeometry, outsideFvGeometry, center, insideScv, outsideScv, insideVolVars, outsideVolVars, diffCoeffAvgType);

    auto diffusiveFlux = diffusiveMolecularFluxFicksLaw_(domainI, domainJ, center, domainIPhaseIdx, domainJPhaseIdx, insideScv, outsideScv, insideVolVars, outsideVolVars, diffCoeffAvgType);

    for (int compIdx = 0; compIdx < diffusiveFlux.size(); ++compIdx)
    {
        const int domainICompIdx = couplingCompIdx(domainI, compIdx);
        const int domainJCompIdx = couplingCompIdx(domainJ, compIdx);

        const bool insideDiffFluxIsUpstream = std::signbit(diffusiveFlux[domainICompIdx]);
        const Scalar componentEnthalpy = insideDiffFluxIsUpstream ?
                                         getComponentEnthalpy(insideVolVars, domainIPhaseIdx, domainICompIdx)
                                       : getComponentEnthalpy(outsideVolVars, domainJPhaseIdx, domainJCompIdx);

        // always use a mass-based calculation for the energy balance
        diffusiveFlux[domainICompIdx] *= FluidSystem<i>::molarMass(domainICompIdx);

        flux += diffusiveFlux[domainICompIdx] * componentEnthalpy;
    }

    return flux;
}

};

} // end namespace Dumux

#endif // DUMUX_STOKES_DROPS_DARCY_COUPLINGDATA_HH
