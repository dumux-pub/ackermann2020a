install(FILES
couplingdata.hh
couplingmanager.hh
couplingmapper.hh
dropmanager_1pc.hh
dropmanager_npc.hh
newtonsolver.hh
DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/src/coupling)
