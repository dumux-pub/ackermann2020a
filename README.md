This module contains all required files to reproduce the results published in the PhD thesis "A multi-scale approach for drop/porous-medium interaction" by S. Ackermann (2021).

Installation
============
You need to have gcc/c++ 5 installed to compile and run this module.

The easiest way to install this module and its dependencies is to create a new directory and clone this module:

```
mkdir Ackermann2020a && cd Ackermann2020a
git clone https://git.iws.uni-stuttgart.de/dumux-pub/ackermann2020a.git
```

After that, execute the file [installAckermann2020a.sh](https://git.iws.uni-stuttgart.de/dumux-pub/ackermann2020a/raw/master/installAckermann2020a.sh)

```
chmod u+x ackermann2020a/installAckermann2020a.sh
./ackermann2020a/installAckermann2020a.sh
```

This should automatically download all necessary modules and check out the correct versions.

Navigate to the folder `dumux` and apply the patch `name.patch` 
   ```bash
   cd dumux
   patch -p0 <../ackermann2020a/name.patch
   cd ..
   ```

Finally, run

```
./dune-common/bin/dunecontrol --opts=dumux/cmake.opts all
```

To build and run the executables, navigate to the folder `ackermann2020a/build-cmake/appl/` and continue to the respective subfolder.


Dependencies
=======

dune-common               releases/2.6    1b45bfb24873f80bfc7b419c04dea79f93aef1d7

dune-geometry             releases/2.6    1eb09fa58d4894ead8df4dd235bd3d19f2b6ac38

dune-localfunctions       releases/2.6    ee794bfdfa3d4f674664b4155b6b2df36649435f

dune-grid                 releases/2.6    76f18471498824d49a6cecbfba520b221d9f79ca

dune-istl                 releases/2.6    9698e497743654b6a03c219b2bdfc27b62a7e0b3

dumux                     releases/3.0    ae8ae87ddc39d02795ef11ad5744c8e85f334a53


Installation with Docker
========================

Create a new folder in your favourite location and change into it

```bash
mkdir Ackermann2020a
cd Ackermann2020a
```

Download the container startup script
```bash
wget https://git.iws.uni-stuttgart.de/dumux-pub/ackermann2020a/-/raw/master/docker_ackermann2020a.sh
```

Open the Docker Container
```bash
bash docker_ackermann2020a.sh open
```

After the script has run successfully, you may build all executables

```bash
cd ackermann2020a/build-cmake
make build_tests
```

and you can run them individually. They are located in the build-cmake folder according to the following paths:

- appl/experiments
- appl/1p2c_2p2c_lateral
- appl/1p2c_2p2c_detach
- appl/1p2c_2p2c_detach/simplecoupling

It can be executed with an input file, e.g.

```bash
cd appl/experiments
./test_exp params.input
```
